package com.ourfuture.junitdemo.demo07;

public class AccountService {
	private AccountManager accountManager;

	public void setAccountManager(AccountManager manager) {
		this.accountManager = manager;
	}

	public void transfer(String senderId, String beneficaryId, long amount) {
		Account sender = this.accountManager.findAccountForUser(senderId);
		Account beneficiary = this.accountManager.findAccountForUser(beneficaryId);
		sender.debit(amount);
		beneficiary.credit(amount);
		this.accountManager.updateAccount(sender);
		this.accountManager.updateAccount(beneficiary);
	}
}
