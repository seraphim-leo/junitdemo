package com.ourfuture.junitdemo.demo03;

public interface RequestHandler {
	Response process(Request request) throws Exception ;
}
