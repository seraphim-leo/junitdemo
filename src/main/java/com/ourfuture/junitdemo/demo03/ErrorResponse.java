package com.ourfuture.junitdemo.demo03;

public class ErrorResponse implements Response {
	private Request originalRequest;
	private Exception originalException;

	public ErrorResponse(Request request, Exception exception) {
		this.originalRequest = request;
		this.originalException = exception;
	}

	public String getName() {
		return "ErrorResponse";
	}

	public Request getOriginalRequest() {
		return originalRequest;
	}

	public Exception getOriginalException() {
		return originalException;
	}

}
