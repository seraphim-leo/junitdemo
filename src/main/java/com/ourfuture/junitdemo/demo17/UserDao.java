package com.ourfuture.junitdemo.demo17;

public interface UserDao {
	long addUser(User user);

	User getUserById(long id);
}
