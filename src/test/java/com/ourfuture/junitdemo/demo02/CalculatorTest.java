package com.ourfuture.junitdemo.demo02;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class CalculatorTest {

	private double expected;
	private double valueOne;
	private double valueTwo;

	private Calculator calculator;

	@Before
	public void prepare() {
		calculator = new Calculator();
	}

	@Parameters
	public static Collection<Integer[]> getTestParameters() {
		return Arrays.asList(new Integer[][] { { 2, 1, 1 }, {3, 2, 1 },
				{ 5, 3, 2 } });
	}

	public CalculatorTest(double expected, double valueOne, double valueTwo) {
		this.expected = expected;
		this.valueOne = valueOne;
		this.valueTwo = valueTwo;
	}

	@Test
	public void testAdd() {
		assertEquals(expected, calculator.add(valueOne, valueTwo), 0);
	}
}
