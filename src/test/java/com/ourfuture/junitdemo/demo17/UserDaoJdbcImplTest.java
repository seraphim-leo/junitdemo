package com.ourfuture.junitdemo.demo17;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;

public class UserDaoJdbcImplTest extends AbstratDbUnitTestCase {

	private static UserDaoJdbcImpl dao = new UserDaoJdbcImpl();

	@Test
	public void testGetUserById() throws Exception {

		IDataSet setupDataSet = getDataSet("/user.xml");
		DatabaseOperation.CLEAN_INSERT.execute(dbunitConnection, setupDataSet);

		dao.setConnection(connection);
		User user = dao.getUserById(1);

		assertNotNull(user);
		assertEquals("admin", user.getName());
	}

	@Test
	public void testAddUser() throws Exception {
		User user = new User();
		user.setId(5);
		user.setName("test");

		long id = dao.addUser(user);

		assertTrue(id > 0);
		assertEquals(id, user.getId());
		IDataSet expectedDataSet = getDataSet("/user.xml");
		IDataSet actualDataSet = dbunitConnection.createDataSet();
		Assertion.assertEquals(expectedDataSet, actualDataSet);
	}
}
