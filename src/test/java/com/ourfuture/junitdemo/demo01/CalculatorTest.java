package com.ourfuture.junitdemo.demo01;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

	private Calculator calculator;

	@Before
	public void prepare() {
		calculator = new Calculator();
	}

	@Test
	public void testAdd() {
		double result = calculator.add(10, 50);
		assertEquals(60, result, 0);
	}
}
